#!/bin/sh

git pull

rm -f Bin/ChatGameServer/ChatGameServer

docker stop chat_game_svr
docker rm chat_game_svr

docker build -t chat_game:latest .
docker rmi -f $(docker images | grep "<none>" | awk "{print \$3}")
docker run -d --name chat_game_svr --net=host -v /data/ChatGameService/Log:/release/Log -v /etc/timezone:/etc/timezone:ro -v /etc/localtime:/etc/localtime:ro  chat_game:latest

cd Log
tail -f info.log