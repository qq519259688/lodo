if [ $# -ne 2 ];
then
        echo "usage: sh run.sh [start|stop|restart] [test]"
        exit 0
fi

ver=$2

ulimit -c unlimited
if [[ $1 == "stop" ]]; then
        ps -ef | grep tag=${ver} | grep -v grep  | awk '{print $2}' | xargs kill -15
elif [[ $1 == "start" ]]; then
        cd GameServer
        GOTRACEBACK=crash ./GameServer --tag=${ver} &
elif [[ $1 == "restart" ]]; then
        ps -ef | grep tag=${ver} | grep -v grep  | awk '{print $2}' | xargs kill -15
        sleep 5s
        cd GameServer
        GOTRACEBACK=crash ./GameServer --tag=${ver} &
else
        echo "usage: sh run.sh [start|stop|restart] [test]"
        exit 0
fi
