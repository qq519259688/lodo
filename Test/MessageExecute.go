package main

import (
	log "Server-Core/Server/Base/Log"
	message "Server-Core/Server/Message"
	pb "Server-Core/Server/Message"
	"encoding/binary"
	"github.com/golang/protobuf/proto"
	"time"
)

var count = 0

func MessageHandle(client *client, data []byte) {
	log.Info("-----------------------", data)
	//2个字节之前
	msgId := int(binary.LittleEndian.Uint16(data[:2]))
	log.Info("Id: %d recv Message Cmd: %s len:%d", client.id, pb.MSGID_name[int32(msgId)], len(data))

	//req := &message.DataRsp{}
	req := &message.LoginReq{}
	//rsp := &message.DataRsp{}
	//	rsp.Result = uint32(message.GameStatus_GAME_STATUS_OK)
	////2个字节之后
	data2 := data[2:]
	proto.Unmarshal(data2, req) //解包

	log.Info("req===", req)
	//log.Info("Id: %d recv Message Cmd: %d len:%d", client.id, int32(msgId), len(data))
	frame := make([]byte, len(data)-2)
	copy(frame, data[2:])
	info, ok := client.cmdInfo[pb.MSGID(msgId)]

	if ok && len(info) > 0 {
		client.responseTime = append(client.responseTime, time.Now().UnixNano()-info[0])
		client.cmdInfo[pb.MSGID(msgId)] = info[1:]
	}
}
