package main

import (
	pb "Server-Core/Server/Message"
	"bufio"
	"encoding/binary"
	"io"
	"log"
	"math/rand"
	"net"
	"strconv"
	"time"
)

type client struct {
	id            int
	conn          *net.TCPConn
	running       bool
	recv          chan interface{}
	lastHeartbeat int64
	nextCmdTime   int64
	cmdInfo       map[pb.MSGID][]int64
	responseTime  []int64

	totalSize  uint32
	buffer     []byte
	bufferSize uint32
}

func CreateClient(host string, port int, id int) (*client, error) {
	c := new(client)
	c.id = id
	c.lastHeartbeat = time.Now().Unix()
	c.cmdInfo = make(map[pb.MSGID][]int64)
	var tcpAddr *net.TCPAddr
	address := host + ":" + strconv.Itoa(port)
	tcpAddr, _ = net.ResolveTCPAddr("tcp", address)
	var err error
	c.conn, err = net.DialTCP("tcp", nil, tcpAddr)
	if err != nil {
		return nil, err
	}
	c.running = true
	c.recv = make(chan interface{}, 100)

	c.totalSize = 0
	c.buffer = make([]byte, 2560)
	c.bufferSize = 0
	log.Println("Id: " + strconv.Itoa(id) + " connect success")
	return c, nil
}

func (c *client) Release() {
	c.running = false
}

func (c *client) RandomCmd() {
	//HeroUpgradeReq(c, 1)
}

func (c *client) Run() {
	go c.Receive()

	//LoginReq(c, Uid+"_"+strconv.Itoa(c.id), "b") //玩家登录
	GetUserInfo(c, Uid+"_"+strconv.Itoa(c.id), "b")
	c.nextCmdTime = time.Now().Unix() + int64(rand.Intn(5)+5)
	for c.running {
		select {
		case msg, ok := <-c.recv:
			if !ok {
				c.running = false
				break
			}
			MessageHandle(c, msg.([]byte))
		case <-time.After(100 * time.Millisecond):
			if c.running && time.Now().Unix() >= c.nextCmdTime {
				c.RandomCmd()
				c.nextCmdTime = time.Now().Unix() + int64(rand.Intn(10)+5)
			}
		}

		if time.Now().Unix()-c.lastHeartbeat >= 10 {
			HeartbeatPush(c)
			c.lastHeartbeat = time.Now().Unix()
		}
	}

	c.conn.Close()
	//log.Println("Id: " + strconv.Itoa(c.id) + " disconnect")
	WG.Done()
}

func (c *client) Receive() {
	WG.Add(1)
	reader := bufio.NewReader(c.conn)
	for c.running {
		b, err := reader.ReadByte()
		if err == io.EOF {
			close(c.recv)
			break
		}

		if err != nil {
			continue
		}

		c.buffer[c.bufferSize] = b
		c.bufferSize += 1
		if c.totalSize == 0 && c.bufferSize == 4 {
			c.totalSize = binary.LittleEndian.Uint32(c.buffer) + 4
		} else if c.totalSize == c.bufferSize {
			newBuffer := make([]byte, 2560)
			copy(newBuffer, c.buffer)
			c.recv <- newBuffer[4:c.totalSize]
			c.totalSize = 0
			c.bufferSize = 0
		}
	}
	WG.Done()
}
