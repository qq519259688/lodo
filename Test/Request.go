package main

import (
	pb "Server-Core/Server/Message"
	"bytes"
	"encoding/binary"
	"github.com/golang/protobuf/proto"
	"time"
)

func SendMsg(client *client, cmd pb.MSGID, bodyData []byte) {
	msgId := make([]byte, 2)
	binary.LittleEndian.PutUint16(msgId, uint16(cmd))
	totalBuf := bytes.Join([][]byte{msgId, bodyData}, []byte(""))
	sizeBuf := make([]byte, 4)
	binary.LittleEndian.PutUint32(sizeBuf, uint32(len(totalBuf)))
	totalBuf = bytes.Join([][]byte{sizeBuf, totalBuf}, []byte(""))
	client.conn.Write(totalBuf)
	if cmd != pb.MSGID_MSGID_Heartbeat_Push {
		client.cmdInfo[cmd] = append(client.cmdInfo[cmd], time.Now().UnixNano())
	}

	//log.Printf("Id: %d send Message Cmd: %s", client.id, pb.MSGID_name[int32(cmd)])
}

// 登录请求
func LoginReq(client *client, uid string, channel string) {
	body := &pb.LoginReq{}
	body.Uid = "liangfan1000"
	body.PassWd = "liangfan100"
	bodyData, _ := proto.Marshal(body)
	SendMsg(client, pb.MSGID_MSGID_Login, bodyData)
}

func GetUserInfo(client *client, uid string, channel string) {
	body := &pb.LoginReq{}
	body.Uid = "1"
	body.PassWd = "liangfan100"
	proto.Marshal(body)

	//SendMsg(client, pb.MSGID_Get_User_Info_Req, bodyData)
}

// 心跳包
func HeartbeatPush(client *client) {
	body := &pb.HeartbeatPush{}
	bodyData, _ := proto.Marshal(body)
	SendMsg(client, pb.MSGID_MSGID_Heartbeat_Push, bodyData)
}

// 拉取房间请求
func GetRoomListReq(client *client) {
	body := &pb.GetRoomListReq{}
	bodyData, _ := proto.Marshal(body)
	SendMsg(client, pb.MSGID_MSGID_Get_Room_List, bodyData)
}
