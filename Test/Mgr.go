/*
@Time : 2020/9/21 11:04
@Author : xuhanlin
@File : Mgr.go
@Description : 客户端管理
*/

package main

import (
	base "Server-Core/Server/Base"
	"fmt"
	"math/rand"
	"os"
	"os/signal"
	"strconv"
	"sync"
	"syscall"
	"time"
)

var G *mgr
var WG sync.WaitGroup
var Uid = "harley233"

type mgr struct {
	inc          *base.AutoInc
	host         string
	port         int
	clientNum    int
	clientById   map[int]*client
	running      bool
	clientRecord [][]int64
	startTime    int64
}

func CreateMgr(host string, port int, clientNum int) {
	G = new(mgr)
	G.host = host
	G.port = port
	G.clientNum = clientNum
	G.clientById = make(map[int]*client)
	G.inc = base.NewAutoInc(1, 1)
	G.running = true
	G.startTime = time.Now().Unix()
	rand.Seed(time.Now().UnixNano())

	for i := 0; i < G.clientNum; i++ {
		id := G.inc.Id()
		client, err := CreateClient(G.host, G.port, id)
		if err != nil {
			fmt.Println("创建失败, ID:" + strconv.Itoa(id) + ", ERR:" + err.Error())
		} else {
			G.clientById[id] = client
			WG.Add(1)
			go client.Run()
		}
	}
}

func ReleaseMgr() {
	for _, v := range G.clientById {
		v.Release()
	}
	G.running = false
}

func Run() {
	stop := make(chan os.Signal, 1)
	signal.Notify(stop, syscall.SIGINT, syscall.SIGTERM)
	for G.running {
		select {
		case <-time.After(time.Second):
			for id, client := range G.clientById {
				if client.running == false {
					delete(G.clientById, id)
				}
			}

			//log.Println("###### clientNum: " + strconv.Itoa(len(G.clientById)))
			if len(G.clientById) == 0 {
				G.running = false
			}
		case <-stop:
			ReleaseMgr()
		}
	}

	WG.Wait()
}
