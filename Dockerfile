FROM golang:1.19-alpine as builder

ENV GOPROXY https://goproxy.cn
ENV GO111MODULE on

WORKDIR /go/cache

ADD go.mod .
ADD go.sum .
RUN go mod download

WORKDIR /go/release
ADD . .
WORKDIR /go/release/Server/GameServer
RUN go build -o /go/release/Bin/GameServer/GameServer

FROM alpine
COPY --from=builder /go/release/Bin /release
WORKDIR /release/GameServer

EXPOSE 20001
CMD ["./GameServer", "--tag=dev"]
