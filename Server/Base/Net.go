package base

import (
	"Server-Core/Server/Base/Log"
	"github.com/panjf2000/gnet"
	"time"
)

type Net struct {
	*gnet.EventServer
}

func (handle *Net) OnInitComplete(srv gnet.Server) (action gnet.Action) {
	log.Info("开始监听端口 addr:%s, multi-cores:%t,loops:%d", srv.Addr.String(), srv.Multicore, srv.NumEventLoop)
	return
}

func (handle *Net) OnShutdown(srv gnet.Server) {
	log.Info("OnShutdown addr:%s, multi-cores:%t,loops:%d", srv.Addr.String(), srv.Multicore, srv.NumEventLoop)
}

func (handle *Net) OnOpened(c gnet.Conn) (out []byte, action gnet.Action) {
	log.Info("OnOpened localAddr:%d, remoteAddr:%s", c.LocalAddr().String(), c.RemoteAddr().String())
	return
}

func (handle *Net) OnClosed(c gnet.Conn, err error) (action gnet.Action) {
	return
}

func (handle *Net) React(frame []byte, c gnet.Conn) (out []byte, action gnet.Action) {
	return
}

func (handle *Net) PreWrite() {
}

func (handle *Net) Tick() (delay time.Duration, action gnet.Action) {
	return
}
