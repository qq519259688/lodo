package base

type ServerExecute interface {
	Run()
	LogicStart()
	LogicStep()
	OnServerReady()
	LogicEnd()
}
