package log

import (
	"github.com/sirupsen/logrus"
	"io/ioutil"
)

type Level string

const (
	DebugLevel Level = "DEBUG"
	InfoLevel  Level = "INFO"
	WarnLevel  Level = "WARN"
	ErrorLevel Level = "ERROR"
)

var logger *logrus.Logger = nil

func init() {
	logger = logrus.New()
	logger.SetFormatter(&LineFormatter{
		Skip: 10,
	})

	logger.SetLevel(logrus.DebugLevel)
	return
}

// -------------------config----------------------

func SetLevel(level Level) {
	lvl, _ := logrus.ParseLevel(string(level))
	logger.SetLevel(lvl)
}

func ConsoleOff() {
	logger.SetOutput(ioutil.Discard)
}

func AddRotateHook(path string, day uint, pass bool) {
	for level := logger.GetLevel(); level >= logrus.ErrorLevel; level-- {
		logger.AddHook(NewRotateHook(level, path, day, pass))
	}
}

// -------------------print----------------------

func Debug(str string, args ...interface{}) {
	logger.Debugf(str, args...)
}

func Info(str string, args ...interface{}) {
	logger.Infof(str, args...)
}

func Warn(str string, args ...interface{}) {
	logger.Warnf(str, args...)
}

func Error(str string, args ...interface{}) {
	logger.Errorf(str, args...)
}
