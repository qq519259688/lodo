/*
@Time : 2020/9/8 10:01
@Author : xuhanlin
@File : Def.go
@Description :
*/

package service

import "sync"

type Msg struct {
	Cmd  string
	Args []interface{}
	Sync bool
	Done chan []interface{}
}

type Handler interface {
	Init(ch chan Msg, wg *sync.WaitGroup, args ...interface{}) error
	UnInit()
	Shutdown()
	Status() bool
	Run()
}
