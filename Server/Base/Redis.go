package base

import (
	"Server-Core/Server/Base/Log"
	"context"
	"github.com/go-redis/redis/v8"
)

type RdbInit struct {
	Addr     string
	DB       int
	Password string
}

type Rdb struct {
	client *redis.Client
	ctx    context.Context
}

func (rdb *Rdb) Create(init *RdbInit) error {
	rdb.client = redis.NewClient(&redis.Options{
		Addr:     init.Addr,
		Password: init.Password,
		DB:       init.DB,
	})

	rdb.ctx = context.Background()
	_, err := rdb.client.Ping(rdb.ctx).Result()
	if err != nil {
		log.Error("Redis Create Error:%s", err.Error())
	}
	return err
}
