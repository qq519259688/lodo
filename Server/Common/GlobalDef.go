package common

type Message struct {
	F    func(int, []byte)
	Id   int
	Data []byte
}

type ServerConf struct {
	GameName   string
	ServerName string
	Debug      bool
	ClientPort int
	DBHost     string
	DBPort     string
	DBUser     string
	DBPassword string
	DBName     string
	LogLevel   string
	GMPort     int
	WEBPort    string
	BaseURL    string
}
