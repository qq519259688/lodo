package scheme

import (
	"Server-Core/Server/Base"
	"errors"
	"reflect"
	"strconv"
)

type LevelItem struct {
	Level            int32
	Exp              int32
	HeroMaxLevel     int32
	MaxEnergy        int32
	MaxStorageEnergy int32
	LevelPrize       string
}

type Level struct {
	dataList map[int32]*LevelItem
}

func (scp *Level) Load() {
	scp.dataList = make(map[int32]*LevelItem)
	//records := base.LoadCsvCfg("../Scp/Level.csv").Records

	//records := base.LoadCsvCfg("E://ChatGameService2/Bin/Scp/Level.csv").Records
	var path string = ""
	if ip == "192.168.1.122" {
		path = "E://ChatGameService2/Bin/Scp/Level.csv"
	} else {
		//	records := base.LoadCsvCfg("E://ChatGameService2/Bin/Scp/Level.csv").Records
		path = "../Scp/Level.csv"
	}
	records := base.LoadCsvCfg(path).Records
	for i := 4; i < len(records); i++ {
		item := new(LevelItem)
		cr := reflect.ValueOf(item).Elem()
		for k, v := range records[i].Record {
			k = base.StrFirstToUpper(k)
			a := cr.FieldByName(k)
			switch a.Type().String() {
			case "string":
				a.Set(reflect.ValueOf(v))
			case "int32":
				i, _ := strconv.Atoi(v)
				a.Set(reflect.ValueOf(int32(i)))
			case "float64":
				f, _ := strconv.ParseFloat(v, 64)
				a.Set(reflect.ValueOf(f))
			}
		}
		scp.dataList[item.Level] = item
	}
}

func (scp *Level) Get(key int32) (interface{}, error) {
	data, ok := scp.dataList[key]
	if ok {
		return data, nil
	} else {
		return nil, errors.New("not find")
	}
}

func (scp *Level) GetMaxLevel() int32 {
	var maxLevel int32 = 0
	for _, item := range scp.dataList {
		if item.Level > maxLevel {
			maxLevel = item.Level
		}
	}
	return maxLevel
}
