package scheme

type ID int32
type G int32

const (
	ID_Min = iota
	ID_Item
	ID_Level
	ID_Role
	ID_RoleLevel
	ID_Sz
	ID_Max
)

type Scp interface {
	Load()
	Get(key int32) (interface{}, error)
}

func Register() {
	mgr.schemeList[ID_Item] = new(Item)
	mgr.schemeList[ID_Level] = new(Level)
	mgr.schemeList[ID_Role] = new(Role)
	mgr.schemeList[ID_RoleLevel] = new(RoleLevel)
	mgr.schemeList[ID_Sz] = new(Sz)
}
