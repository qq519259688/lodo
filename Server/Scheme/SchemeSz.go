package scheme

import (
	"Server-Core/Server/Base"
	"reflect"
	"strconv"
)

// ds,weight,playerNum,finialPlane,finialArea,finialPoint
type SzItem struct {
	Ds          int32 //点数
	Weight      int32 //权重
	PlayerNum   int32 //游戏人数(是否是4个人玩，1是，2否)
	FinialPlane int32 //是否最后一架飞机（1是，2否）
	FinialArea  int32 //是否到达终点区（1是，2否）
	FinialPoint int32 //到达终点区所需要的点数
}

type Sz struct {
	DataList []*SzItem
}

func (scp *Sz) Load() {
	//var dataList = []*SzItem{}
	//	scp.dataList = make(map[int32]*SzItem)
	//records := base.LoadCsvCfg("../Scp/Level.csv").Records

	//records := base.LoadCsvCfg("E://ChatGameService2/Bin/Scp/Level.csv").Records
	var path string = ""
	if ip == "192.168.1.122" {
		//path = "E://ChatGameService2/Bin/Scp/Level.csv"
		path = "E://ChatGameService2/Bin/Scp/Sz.csv"
	} else {
		//	records := base.LoadCsvCfg("E://ChatGameService2/Bin/Scp/Level.csv").Records
		path = "../Scp/Sz.csv"
	}
	records := base.LoadCsvCfg(path).Records
	for i := 4; i < len(records); i++ {
		item := new(SzItem)
		cr := reflect.ValueOf(item).Elem()
		for k, v := range records[i].Record {
			k = base.StrFirstToUpper(k)
			a := cr.FieldByName(k)
			/*	switch a.Type().String() {
				case "string":
					a.Set(reflect.ValueOf(v))
				case "int32":
					i, _ := strconv.Atoi(v)
					a.Set(reflect.ValueOf(int32(i)))
				case "float64":
					f, _ := strconv.ParseFloat(v, 64)
					a.Set(reflect.ValueOf(f))
				}*/
			i, _ := strconv.Atoi(v)
			//	log.Info("i", i)
			a.Set(reflect.ValueOf(int32(i)))
		}
		//scp.dataList[item.Level] = item
		scp.DataList = append(scp.DataList, item)
		//log.Info("item", item)
	}
}

func (scp *Sz) Get(key int32) (interface{}, error) {
	/*data, ok := scp.dataList[key]
	if ok {
		return data, nil
	} else {
		return nil, errors.New("not find")
	}*/
	return nil, nil
}

func (scp *Sz) LoadData() {
	var dataList = []*SzItem{}
	//var sz =Sz{};

	//	scp.dataList = make(map[int32]*SzItem)
	//records := base.LoadCsvCfg("../Scp/Level.csv").Records

	//records := base.LoadCsvCfg("E://ChatGameService2/Bin/Scp/Level.csv").Records
	var path string = ""
	if ip == "192.168.1.122" {
		//path = "E://ChatGameService2/Bin/Scp/Level.csv"
		path = "E://ChatGameService2/Bin/Scp/Sz.csv"
	} else {
		//	records := base.LoadCsvCfg("E://ChatGameService2/Bin/Scp/Level.csv").Records
		path = "../Scp/Sz.csv"
	}
	records := base.LoadCsvCfg(path).Records
	for i := 4; i < len(records); i++ {
		item := new(SzItem)
		cr := reflect.ValueOf(item).Elem()
		for k, v := range records[i].Record {
			k = base.StrFirstToUpper(k)
			a := cr.FieldByName(k)
			/*	switch a.Type().String() {
				case "string":
					a.Set(reflect.ValueOf(v))
				case "int32":
					i, _ := strconv.Atoi(v)
					a.Set(reflect.ValueOf(int32(i)))
				case "float64":
					f, _ := strconv.ParseFloat(v, 64)
					a.Set(reflect.ValueOf(f))
				}*/
			i1, _ := strconv.Atoi(v)
			//	log.Info("i", i)
			a.Set(reflect.ValueOf(int32(i1)))
		}
		//scp.dataList[item.Level] = item
		dataList = append(dataList, item)
		//log.Info("item", item)
	}
	scp.DataList = dataList
	//return sz
}
