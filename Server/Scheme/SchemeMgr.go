package scheme

import log "Server-Core/Server/Base/Log"

var mgr *Mgr

type Mgr struct {
	schemeList [ID_Max]Scp
}

func Create() {
	mgr = new(Mgr)
	Register()
	Load()
}

func Load() {
	for i := ID_Min + 1; i < ID_Max; i++ {
		mgr.schemeList[i].Load()
	}
}

func GetSchemeById(id ID) Scp {
	log.Info("id", id)
	log.Info("mgr", mgr)
	return mgr.schemeList[id]
}
