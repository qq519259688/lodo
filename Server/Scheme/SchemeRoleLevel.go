package scheme

import (
	"Server-Core/Server/Base"
	"errors"
	"reflect"
	"strconv"
)

type RoleLevelItem struct {
	Level int32
	Cost  string
	Id    int32
	Role  int32
}

type RoleLevel struct {
	dataList map[int32]*RoleLevelItem
}

func (scp *RoleLevel) Load() {
	scp.dataList = make(map[int32]*RoleLevelItem)
	//records := base.LoadCsvCfg("../Scp/RoleLevel.csv").Records
	//records := base.LoadCsvCfg("./Scp/RoleLevel.csv").Records
	var path string = ""
	if ip == "192.168.1.122" {
		path = "E://ChatGameService2/Bin/Scp/RoleLevel.csv"
	} else {
		//	records := base.LoadCsvCfg("E://ChatGameService2/Bin/Scp/Level.csv").Records
		path = "../Scp/RoleLevel.csv"
	}
	records := base.LoadCsvCfg(path).Records
	//records := base.LoadCsvCfg("E://ChatGameService2/Bin/Scp/RoleLevel.csv").Records
	for i := 4; i < len(records); i++ {
		item := new(RoleLevelItem)
		cr := reflect.ValueOf(item).Elem()
		for k, v := range records[i].Record {
			k = base.StrFirstToUpper(k)
			a := cr.FieldByName(k)
			switch a.Type().String() {
			case "string":
				a.Set(reflect.ValueOf(v))
			case "int32":
				i, _ := strconv.Atoi(v)
				a.Set(reflect.ValueOf(int32(i)))
			case "float64":
				f, _ := strconv.ParseFloat(v, 64)
				a.Set(reflect.ValueOf(f))
			}
		}
		scp.dataList[item.Id] = item
	}
}

func (scp *RoleLevel) Get(key int32) (interface{}, error) {
	data, ok := scp.dataList[key]
	if ok {
		return data, nil
	} else {
		return nil, errors.New("not find")
	}
}
