package main

import (
	base "Server-Core/Server/Base"
	"Server-Core/Server/GameServer/Server"
)

func main() {
	gameServer := new(server.GameServer)
	base.NewServer(gameServer)
}
