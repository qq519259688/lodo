/*
@Time : 2020/9/8 19:37
@Author : xuhanlin
@File : Execute.go
@Description :
*/

package logic

import (
	log "Server-Core/Server/Base/Log"
	service "Server-Core/Server/Base/Service"
	db "Server-Core/Server/GameServer/Server/DB"
	message "Server-Core/Server/Message"
	"github.com/golang/protobuf/proto"

	//"strconv"
	"time"
)

var execute map[string]func([]interface{}) []interface{}
var clientCmd map[message.MSGID]func(int, []byte)

func register() {
	execute = make(map[string]func([]interface{}) []interface{})
	execute["GMOnHandle"] = GMOnHandle
	//业务逻辑
	clientCmd = make(map[message.MSGID]func(int, []byte))
	clientCmd[message.MSGID_MSGID_Heartbeat_Push] = HeartBeatReq
	clientCmd[message.MSGID_MSGID_Login] = LoginReq
	//获取用户信息
	//clientCmd[message.MSGID_MSGID_User_Info] = GetUserInfo
	//clientCmd[message.MSGID_Throw_The_Dice] = ThrowTheDice
	//clientCmd[message.MSGID_Move] = Move
}

func HeartBeatReq(clientId int, data []byte) {
	player := G.playerMng.GetPlayerByClientId(clientId)
	if player != nil {
		player.LastHeartbeat = time.Now().Unix()
	}
}

// 登录流程，只是个模版
func LoginReq(clientId int, data []byte) {
	log.Info("rece====LoginReq")
	req := &message.LoginReq{}
	rsp := &message.LoginRsp{}
	rsp.Result = uint32(message.GameStatus_GAME_STATUS_OK)
	var player *Player = nil
	for {
		err := proto.Unmarshal(data, req) //解包
		if err != nil {
			log.Error("Unmarshal fail clientId:%d err:%s", clientId, err.Error())
			rsp.Result = uint32(message.GameStatus_GAME_STATUS_PB_ERR)
			break
		}
		//测试期间：没有账号则创建账号和密码，有账号则验证密码
		log.Info("LoginReq recive clientId:%d Req={%s}", clientId, req)
		//先验证账号和密码不能为空
		if len(req.Uid) == 0 || len(req.PassWd) == 0 {
			log.Error("clientId:%d Login err uid:%s passwd:%s", clientId, req.Uid, req.PassWd)
			rsp.Result = uint32(message.GameStatus_GAME_STATUS_UID_PASSWD_NIL)
			break
		}

		account := db.Account{}
		db.QueryPlayerByUid(&account, req.Uid) //查询玩家是否存在能确定唯一RoleId
		playerId := account.PlayerId
		//验证账号
		if playerId != 0 && req.PassWd != account.PassWd {
			log.Error("Login passwd error clientId:%d account.PassWd:%s req.PassWd:%s", clientId, account.PassWd, req.PassWd)
			rsp.Result = uint32(message.GameStatus_GAME_STATUS_PASSWD_ERROR)
			break
		}
		player = G.playerMng.GetPlayerByPlayerId(playerId) //玩家缓存不再则new一个新玩家，如果存在则顶号
		if player != nil {
			G.playerMng.KickOld(player, clientId) //顶号
		} else {
			player, err = G.playerMng.NewPlayer(req.Uid, req.PassWd, clientId, &account) //new一个新玩家放到内存中
			if err != nil {
				log.Error("Login NewPlayer clientId:%d  error:%s", clientId, err.Error())
				rsp.Result = uint32(message.GameStatus_GAME_STATUS_INNER_ERR)
				break
			}
		}
		player.AfterLoad()

		player.FillLoginRsp(rsp)
		break
	}
	//登录的回包打印，客户端提到登录回包，有时候玩家ID为0
	log.Info("login send clientId:%d Uid:%s rsp:%s", clientId, req.Uid, rsp)
	rData, ok := proto.Marshal(rsp) //回包
	if ok == nil {
		if player != nil {
			player.SendMsg(message.MSGID_MSGID_Login, rData)
		} else {
			//可能密码错误，没获取到玩家指针也要发送错误码的
			SendMsgByClientId(clientId, message.MSGID_MSGID_Login, rData)
		}
	}
}

// 查询房间列表
func GetRoomList(clientId int, data []byte) {
	rsp := &message.GetRoomListRsp{}

	var player *Player = nil
	for {
		player = G.playerMng.GetPlayerByClientId(clientId) //通过客户端链接找到玩家是否存在？
		if player == nil {
			log.Error("GetRoomList player nil clientId:%d", clientId)
			service.Send("clientMgr", "kick", clientId, message.KickReason_KICK_REASON_NOT_LOGIN)
			break
		}
		log.Info("GetRoomList recive PlayerId:%d GetRoomListReq", player.PlayerId)

		break
	}

	if player != nil {
		rsp = G.roomMng.GetRoomList() //
		log.Info("send GetRoomList PlayerId:%d rsp::%s", player.PlayerId, rsp)
		rData, ok := proto.Marshal(rsp) //回包
		if ok == nil {
			player.SendMsg(message.MSGID_MSGID_Get_Room_List, rData)
		}
	}
}
