package logic

type PlayerInfo struct {
	playerId     int64
	name         string
	isFinishLoad bool
	teamId       int64  //队伍ID，其实就是准备房间的ID，用来区分队友
	index        uint32 //玩家序号，从开始
}

type Room struct {
	roomId         int64                 //房间ID
	roomPlayerInfo map[int64]*PlayerInfo //房间玩家信息
	roomMasterId   int64                 //房间归属者ID
	status         int                   //房间状态
	roomType       int                   //房间类型
	startGameTick  int64                 //开始游戏的时间戳,20s没有加载完则直接进入游戏
	mateTick       int64                 //匹配时间，超时则重新回到房间

	tick      int64 //战斗开启后过去的秒数
	creatTime int64 //创建房间时间
}

var times int64 = 0

func (room *Room) Run() {

}

func (room *Room) Init() {

}
