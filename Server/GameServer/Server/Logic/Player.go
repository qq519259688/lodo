package logic

import (
	service "Server-Core/Server/Base/Service"
	"Server-Core/Server/GameServer/Server/DB"
	"Server-Core/Server/Message"
	"time"
)

type Player struct {
	PlayerId      int64
	Name          string
	ClientId      int
	Uid           string
	LastLoginTime int64
	LastHeartbeat int64
}

var badNetworkThreshold int64 = 15

func CreatePlayer() *Player {
	player := new(Player)
	return player
}

func (p *Player) Run() {
}

func (p *Player) Init() {
	p.LastHeartbeat = time.Now().Unix()
}

func (p *Player) Load() {
	p.LastHeartbeat = time.Now().Unix()

}

// 是否处于弱网状态?
func (p *Player) IsWeakNetwork() bool {
	//15s没收到心跳包，表示处于弱网状态
	if time.Now().Unix()-p.LastHeartbeat >= badNetworkThreshold {
		return true
	}
	return false
}

func (p *Player) AfterLoad() {
	db.UpdatePlayer(p.PlayerId)
}

// 填充登录回包数据
func (p *Player) FillLoginRsp(rsp *message.LoginRsp) {
	playerId := p.PlayerId
	rsp.PlayerId = playerId
	//填充登录回包
}

func (p *Player) LoadOther() {
	p.LastHeartbeat = time.Now().Unix()

}

func SendMsgByClientId(clientId int, cmd message.MSGID, data []byte) {
	service.Send("clientMgr", "sendMsgById", clientId, cmd, data)
}

func (p *Player) SendMsg(cmd message.MSGID, data []byte) {
	service.Send("clientMgr", "sendMsgById", p.ClientId, cmd, data)
}
