package logic

import (
	log "Server-Core/Server/Base/Log"
	"encoding/json"
)

type GMOnStatus struct {
}

// GM消息类型
const (
	GM_TYPE_SERVER_STATUS int = 1 //服务器玩家在线状态 GMOnStatus
)

type GMBody struct {
	ErrorCode int64       `json:"errorCode"`
	ErrorMsg  string      `json:"errorMsg"`
	Data      interface{} `json:"data"`
}

// 其他服务调用
func GMOnHandle(args []interface{}) []interface{} {
	cmd := args[0].(int)
	msg_body := args[1].(string)
	var body GMBody
	switch cmd {
	case GM_TYPE_SERVER_STATUS: //服务器玩家在线状态
		body = OnStatus(msg_body)
		break
	default:
		body.ErrorCode = GM_ERROR_FAIL
		body.ErrorMsg = "没有这个消息号"
		break
	}
	return []interface{}{body}
}

// GM错误码类型
const (
	GM_ERROR_SUCESS      int64 = 0     //成功
	GM_ERROR_FAIL        int64 = 20027 //失败
	GM_ERROR_ANALY_ERROE int64 = 20028 //结构体解析错误
)

// 在线状态
func OnStatus(msg_body string) GMBody {
	var req GMOnStatus
	err := json.Unmarshal([]byte(msg_body), &req)
	var rsp GMBody
	if err != nil {
		rsp.ErrorCode = GM_ERROR_ANALY_ERROE
		rsp.ErrorMsg = "结构体解析错误"
		return rsp
	}
	log.Info("Received onStatus:%v", req)
	playerNum := len(G.playerMng.playerByClientId)
	playerNum = 10
	log.Info("playerNum:%d", playerNum)
	rsp.Data = playerNum
	return rsp
}
