package Utils

import scheme "Server-Core/Server/Scheme"

func ReadFile() *scheme.Sz {
	role := scheme.GetSchemeById(scheme.ID_Sz).(*scheme.Sz)
	//role.Load()
	role.LoadData()
	return role
}
