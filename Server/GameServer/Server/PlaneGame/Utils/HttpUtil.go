package Utils

import (
	base "Server-Core/Server/Base"

	"bytes"
	// "strconv"
	// "crypto/md5"
	// "encoding/hex"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	//"strconv"
	"time"
)

//语聊房的代码能跑就不要动，动了就可能跑不掉
func PostData(data []byte) int {
	/*param := string(data)
	fmt.Println("data:", param)
	var key = "dGYGWu5#qaEsQPdD7NnpjD4^BUbF"
	now := time.Now().UnixNano() / 1e6
	s3 := fmt.Sprintf("%s%s%s", param, key, strconv.FormatInt(now, 10))
	fmt.Println("s3",s3)
	re := md5.Sum([]byte(s3))
	md5String := hex.EncodeToString(re[:])
	request, _ := http.NewRequest("POST", "http://47.242.46.203/api/inner/operatingWallet", bytes.NewBuffer(data))
	request.Header.Set("Content-Type", "application/json; charset=UTF-8")
	request.Header.Set("sn", md5String)
	request.Header.Set("t", strconv.FormatInt(now, 10))
	fmt.Println("sn:", md5String)
	fmt.Println("t:", now)
	client := &http.Client{}
	client.Timeout = 2 * time.Second
	response, error := client.Do(request)
	if error != nil {
		log.Print("扣款失败，该玩家余额不足，不能进行游戏", error)
		return 666
	}
	defer response.Body.Close()
	fmt.Println("PostData response Status:", response.Status)
	fmt.Println("response Headers:", response.Header)
	
	body, _ := ioutil.ReadAll(response.Body)
	fmt.Println("response Body:", body)
	fmt.Println("response Body:", string(body))
	var UserInfoRsp UserInfoRsp

	json.Unmarshal([]byte(body), &UserInfoRsp)

	if UserInfoRsp.Code != 200 {
		return 666
	}
	log.Print("data222", UserInfoRsp)
	
	return UserInfoRsp.Code
*/
	return 200
}

type UserInfoReq struct {
	UidSet []int `json:"uidSet"`
}

type UserInfoRsp struct {
	Code int               `json:"code"`
	Msg  string            `json:"msg"`
	Data []UserInfoDataRsp `json:"data"`
}

type UserInfoDataRsp struct {
	UserId    int    `json:"userId"`
	UserNo    int    `json:"userNo"`
	NickName  string `json:"nickName"`
	Avatar    string `json:"avatar"`
	Gender    int    `json:"gender"`
	GoldCoins int    `json:"goldCoins"`
}

func GetUserInfoData(uid int) UserInfoRsp {
	var userInfoReq = UserInfoReq{}
	var uidset = []int{}
	uidset = append(uidset, uid)
	log.Print("uset=====", uidset)
	userInfoReq.UidSet = uidset
	data, _ := json.Marshal(userInfoReq)
	//request, _ := http.NewRequest("POST", base.Server.Config.BaseURL+"api/inner/listUserBasic", bytes.NewBuffer(data))
	request, _ := http.NewRequest("POST", "http://47.242.46.203/api/inner/listUserBasic", bytes.NewBuffer(data))
	request.Header.Set("Content-Type", "application/json; charset=UTF-8")
	client := &http.Client{}
	client.Timeout = 2 * time.Second
	response, error := client.Do(request)
	if error != nil {
		//panic(error)
		log.Printf("获取玩家信息失败查看返回结果", error)
		var UserInfoRsp = UserInfoRsp{}
		UserInfoRsp.Code = 666
		return UserInfoRsp
		//return nifcloud
	}
	defer response.Body.Close()
	fmt.Println("GetUserInfoData response Status:", response.Status)
	fmt.Println("response Headers:", response.Header)
	body, _ := ioutil.ReadAll(response.Body)

	//var articleSlide = map[string]string{}
	//json.Unmarshal([]byte(body), &SqlResponse)
	var UserInfoRsp UserInfoRsp

	json.Unmarshal([]byte(body), &UserInfoRsp)

	log.Print("data222", UserInfoRsp)
	if len(UserInfoRsp.Data) == 0 {
		UserInfoRsp.Code = 666
	}

	return UserInfoRsp
	//log.Print("data2", mapResult["avatar"])
}



func GetFbData(uid int,url string,name string, money uint32) UserInfoRsp {
	var userInfoReq = UserInfoReq{}
	var uidset = []int{}
	uidset = append(uidset, uid)
	log.Print("uset=====", uidset)
	userInfoReq.UidSet = uidset
	//data, _ := json.Marshal(userInfoReq)
	//request, _ := http.NewRequest("POST", base.Server.Config.BaseURL+"api/inner/listUserBasic", bytes.NewBuffer(data))
	// request, _ := http.NewRequest("POST", "http://47.242.46.203/api/inner/listUserBasic", bytes.NewBuffer(data))
	// request.Header.Set("Content-Type", "application/json; charset=UTF-8")
	// client := &http.Client{}
	// client.Timeout = 2 * time.Second
	// response, error := client.Do(request)
	// if error != nil {
	// 	//panic(error)
	// 	log.Printf("获取玩家信息失败查看返回结果", error)
	// 	var UserInfoRsp = UserInfoRsp{}
	// 	UserInfoRsp.Code = 666
	// 	return UserInfoRsp
	// 	//return nifcloud
	// }
	// defer response.Body.Close()
	// fmt.Println("GetUserInfoData response Status:", response.Status)
	// fmt.Println("response Headers:", response.Header)
	// body, _ := ioutil.ReadAll(response.Body)

	//var articleSlide = map[string]string{}
	//json.Unmarshal([]byte(body), &SqlResponse)
	var data=[]UserInfoDataRsp{}
	var UserInfoDataRsp =UserInfoDataRsp{}
	UserInfoDataRsp.GoldCoins=1000000
	
	UserInfoDataRsp.UserId=uid
	UserInfoDataRsp.NickName=name
	UserInfoDataRsp.Avatar=url
	//UserInfoDataRsp.GoldCoins=int(money)
	data = append(data,UserInfoDataRsp )
	var UserInfoRsp UserInfoRsp=UserInfoRsp{Data: data,Code: 200}

	// json.Unmarshal([]byte(body), &UserInfoRsp)

	// log.Print("data222", UserInfoRsp)
	// if len(UserInfoRsp.Data) == 0 {
	// 	UserInfoRsp.Code = 666
	// }


	return UserInfoRsp
	//log.Print("data2", mapResult["avatar"])
}

type RoomReq struct {
	RoomId         int    `json:"roomId"`         //语聊房id
	BroadcastType  int    `json:"broadcastType"`  //⼴播类型: 1.邀请 2.开始游戏 3.结束游戏
	GameType       int    `json:"gameType"`       //游戏类型: 1.ludo
	GameChatRoomId int    `json:"gameChatRoomId"` //游戏房间id
	GameIcon       string `json:"gameIcon"`       //游戏图片
	GameName       string `json:"gameName"`       //游戏名称
}

func ChatRoomBroadcast(roomId int, BroadcastType int, gameChatRoomId int) UserInfoRsp {
	var RoomReq = RoomReq{}
	RoomReq.RoomId = roomId
	RoomReq.BroadcastType = BroadcastType
	RoomReq.GameType = 1
	RoomReq.GameChatRoomId = gameChatRoomId
	RoomReq.GameIcon = "https://vr-1314677574.cos.ap-nanjing.myqcloud.com/icon.png"
	RoomReq.GameName = "lodo"
	data, _ := json.Marshal(RoomReq)
	log.Printf("roomId=====", roomId)
	log.Printf("gameChatRoomId=====", gameChatRoomId)
	request, _ := http.NewRequest("POST", base.Server.Config.BaseURL+"api/inner/chatRoomBroadcast", bytes.NewBuffer(data))
	request.Header.Set("Content-Type", "application/json; charset=UTF-8")
	client := &http.Client{}
	client.Timeout = 2 * time.Second
	response, error := client.Do(request)
	if error != nil {
		//	panic(error)

		//panic(error)
		log.Printf("调用语聊房失败查看返回结果", error)
		var UserInfoRsp = UserInfoRsp{}
		UserInfoRsp.Code = 666
		return UserInfoRsp
		//return nifcloud

	}
	defer response.Body.Close()
	fmt.Println("ChatRoomBroadcast response Status:", response.Status)
	fmt.Println("response Headers:", response.Header)
	body, _ := ioutil.ReadAll(response.Body)
	var UserInfoRsp UserInfoRsp
	fmt.Println("response Body:", string(body))
	json.Unmarshal([]byte(body), &UserInfoRsp)
	return UserInfoRsp

}
