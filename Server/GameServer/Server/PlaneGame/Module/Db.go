package Module

import (
	"fmt"

	_ "github.com/go-sql-driver/mysql"

	"xorm.io/xorm"
	"xorm.io/xorm/names"
)

var Engine *xorm.Engine

func init() {
	var err2 error
	Engine, err2 = xorm.NewEngine("mysql", "root:psGCMSfy9Vds@tcp(8.141.80.205:3306)/chat_game?charset=utf8")
	//maxIdleCount 最大空闲连接数，默认不配置，是2个最大空闲连接
	Engine.SetMaxIdleConns(20)
	//最大连接数，默认不配置，是不限制最大连接数
	Engine.SetMaxOpenConns(100)
	//maxLifetime 连接最大存活时间
	Engine.SetConnMaxLifetime(1000)
	
	if err2 != nil {
		fmt.Println("连接数据库失败", err2)
		return
	}
	// 会在控制台打印执行的sql

	//	XormDb.SetColumnMapper(core.SnakeMapper{})
	Engine.SetTableMapper(names.SnakeMapper{})
	Engine.SetColumnMapper(names.SnakeMapper{})
	Engine.ShowSQL(true)
}