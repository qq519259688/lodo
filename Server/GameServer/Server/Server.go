package server

import (
	base "Server-Core/Server/Base"
	log "Server-Core/Server/Base/Log"
	db "Server-Core/Server/GameServer/Server/DB"
	web "Server-Core/Server/GameServer/Server/PlaneGame/GmWeb"
	module "Server-Core/Server/GameServer/Server/PlaneGame/Module"
	util "Server-Core/Server/GameServer/Server/PlaneGame/Utils"
	message "Server-Core/Server/Message"
	scheme "Server-Core/Server/Scheme"
	"math/rand"
	"time"

)

var GServer *GameServer

type GameServer struct {
}

func (gs *GameServer) Run() {

}

func (gs *GameServer) LogicStart() {
	log.Info("创建schemeMgr")
	scheme.Create()
	log.Info("创建schemeMgr done")

	log.Info("创建logicMgr")
	//	service.Register("logicMgr", new(logic.LogicMgr))
	log.Info("logicMgr done")

	log.Info("创建DB连接")
	config := base.Server.Config
	dbInit := base.DBInit{
		Host:     config.DBHost,
		Port:     config.DBPort,
		User:     config.DBUser,
		Password: config.DBPassword,
		DBName:   config.DBName,
	}
	db.Create(&dbInit)
	log.Info("创建DB连接 done")
	web.Create(config.GMPort) //Gm接口

	rand.Seed(time.Now().UnixNano())
	log.Info("初始化web")
	datainit()
	go web.Webinit(config.WEBPort)
	go web.InitSw()
	go module.ClearSocket()

}

// 数据初始化
func datainit() {
	//Module.GameRoom
	module.RoomInit(0, 100)
	web.CmdExector[int16(message.MSGID_Get_User_Info_Req)] = module.GetUserInfo
	//web.CmdExector[int16(message.MSGID_Get_User_Info_Req2)] = module.GetUserInfo2
	web.CmdExector[int16(message.MSGID_Throw_The_Dice_Req)] = module.ThrowTheDice
	web.CmdExector[int16(message.MSGID_Move_Req)] = module.Move
	web.CmdExector[int16(message.MSGID_Start_Plane_Req)] = module.StartFly
	web.CmdExector[int16(message.MSGID_Trusteeship_Req)] = module.Trusteeship
	web.CmdExector[int16(message.MSGID_MSGID_Heartbeat_Push)] = module.MyHeartBeatReq
	web.CmdExector[int16(message.MSGID_Quit_Req)] = module.QuitGame
	web.CmdExector[int16(message.MSGID_Create_Game_Req)] = module.CreateGameReq
	web.CmdExector[int16(message.MSGID_Join_Room_Req)] = module.JoinRoomReq
	web.CmdExector[int16(message.MSGID_Room_Start_Game_Req)] = module.RoomStartGameReq
	web.CmdExector[int16(message.MSGID_Room_Kick_User_Req)] = module.RoomKickUserReq
	web.CmdExector[int16(message.MSGID_Room_Quit_Req)] = module.RoomQuitReq
	web.CmdExector[int16(message.MSGID_Room_Info_Req)] = module.RoomInfoReq
	web.CmdExector[int16(message.MSGID_Cancel_Match_Req)] = module.CancelMatchReq
	web.CmdExector[int16(message.MSGID_Room_Invite_Req)] = module.RoomInviteReq
	web.CmdExector[int16(message.MSGID_Room_Restart_Req)] = module.RoomRestartReq
	web.CmdExector[int16(message.MSGID_Disc_Req)] = module.DiscReq
	web.CmdExector[int16(message.MSGID_Dissolve_The_Game_Req)] = module.DissolveTheGame
	web.CmdExector[int16(message.MSGID_Close_Game_Req)] = module.RoomCloseReq
	web.CmdExector[int16(message.MSGID_Get_User_Money_Req)] = module.GetUserMoneyReq
	web.CmdExector[int16(message.MSGID_Send_Msg_Req)] = module.SendDataReq
	//初始化文件数据
	sz := util.ReadFile()
	module.DataList = sz.DataList

	module.Initmap2()



	//初始化房间数据

}

func (gs *GameServer) LogicStep() {

}

func (gs *GameServer) OnServerReady() {
	//config := base.Server.Config

	log.Info("创建客户端连接管理")
	//service.Register("clientMgr", new(client.Mgr), config.ClientPort)
	log.Info("服务器启动完成")
	//logic.CreateHighUid(2)
	/*str, result := Sensitive.GetMap().CheckSensitive("我是习近平的大哥胡锦涛")
	log.Info("CheckSensitive str:%s result:%t", str, result)
	target := Sensitive.GetMap().FindAllSensitive("我是习近平的大哥胡锦涛")
	for key, value := range target {
		log.Info("CheckSensitive key:%s value:%d", key, value.Indexes)
	}*/

}

func (gs *GameServer) LogicEnd() {
	//config := base.Server.Config
	log.Info("服务器关闭")
}
