package db

import (
	"time"
)

// 通过账号ID查询玩家是否存在
func UidIsExit(uid string) bool {
	rows := mysql.Query("select * from Account where uid=? ", 1)
	if rows == nil { //不存在
		return false
	}

	defer rows.Close()
	if rows.Next() == false {
		return false
	}
	return true
}

// Account
func QueryPlayerByUid(account *Account, uid string) {
	mysql.Get(account, "select * from Account  where uid = ?", uid)
}

func InsertNewPlayer(uid, passWd string) int64 {
	return mysql.Insert("replace into Account(uid,passWd,createDate,updateDate,lastLoginTime) values(?,?,?,?,?)", uid, passWd, time.Now(), time.Now(), time.Now().Unix())
}

func UpdatePlayer(playerId int64) {
	mysql.Update("update Account set updateDate=?, lastLoginTime=? where playerId=?", time.Now(), 0, playerId)
}
