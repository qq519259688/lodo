package db

import (
	"Server-Core/Server/Base"
)

var mysql *base.Db

type Account struct {
	PlayerId      int64  `db:"playerId"`
	Uid           string `db:"uid"`
	PassWd        string `db:"passWd"`
	Name          string `db:"nickName"`
	CreateDate    string `db:"createDate"`
	UpdateDate    string `db:"updateDate"`
	LastLoginTime int64  `db:"lastLoginTime"`
}

func Create(init *base.DBInit) {
	mysql = new(base.Db)
	mysql.Create(init)
}
