package main

import (
	"bufio"
	"fmt"
	"io"
	"io/ioutil"
	"os/exec"
	"strings"
)

var FileList []string
var SchemeMap map[string]bool

func ExecCmd(name string, arg ...string) bool {
	cmd := exec.Command(name, arg...)
	stdout, err := cmd.StdoutPipe()
	if err != nil {
		fmt.Println(err)
		return false
	}

	stderr, err := cmd.StderrPipe()
	if err != nil {
		fmt.Println(err)
		return false
	}

	err = cmd.Start()
	if err != nil {
		fmt.Println(err)
		return false
	}
	reader := bufio.NewReader(stdout)
	reader2 := bufio.NewReader(stderr)

	for {
		line, err := reader.ReadString('\n')
		if err != nil {
			if err != io.EOF {
				fmt.Println(err)
			}
			break
		}
		fmt.Println(line)
	}

	for {
		line, err := reader2.ReadString('\n')
		if err != nil {
			if err != io.EOF {
				fmt.Println(err)
			}
			break
		}
		fmt.Println(line)
	}

	cmd.Wait()
	return true
}

func main() {
	/*fmt.Println("开始导出配置")
	err := os.Chdir("../../../Tool/Bin")
	if err != nil {
		fmt.Println(err)
		return
	}

	if !ExecCmd("python3", "ConfigSplit.py") {
		return
	}

	err = os.Chdir("../../Server-Core/Tools/conf")
	if err != nil {
		fmt.Println(err)
		return
	}*/

	fmt.Println("开始生成代码")
	files, _ := ioutil.ReadDir("../../Bin/Scp")
	for _, f := range files {
		if f.Name() == "Attributes.csv" {
			a := 1
			a++
		}
		if strings.HasSuffix(f.Name(), "csv") {
			FileList = append(FileList, strings.TrimSuffix(f.Name(), ".csv"))
		}
	}

	SchemeMap = make(map[string]bool)
	//files, _ = ioutil.ReadDir("../../Server/Scheme")
	//E://ChatGameService2/Bin/Config/%s.conf
	files, _ = ioutil.ReadDir("E://ChatGameService2/Server/Scheme")
	for _, f := range files {
		name := strings.TrimPrefix(f.Name(), "Scheme")
		name = strings.TrimSuffix(name, ".go")
		SchemeMap[name] = true
	}

	fmt.Println("开始生成SchemeDef.go")
	if !GeneratorDef() {
		return
	}

	if !GeneratorScp() {
		return
	}

	fmt.Println("生成代码完成")
}
