package main

import (
	"context"
	"fmt"
	"log"
	"net"
	"strings"
	"time"

	"github.com/google/gopacket"
	//	"github.com/google/gopacket/layers"
	"github.com/google/gopacket/layers"
	"github.com/google/gopacket/pcap"
)
type IfaceInfo struct {
	NPFName     string
	Description string
	NickName    string
	IPv4        string
}

func get_if_list() []IfaceInfo {
	var ifaceInfoList []IfaceInfo

	// 得到所有的(网络)设备
	devices, err := pcap.FindAllDevs()
	if err != nil {
		log.Fatal(err)
	}

	interface_list, err := net.Interfaces()
	if err != nil {
		log.Fatal(err)
	}

	for _, i := range interface_list {
		byName, err := net.InterfaceByName(i.Name)
		if err != nil {
			log.Fatal(err)
		}
		address, err := byName.Addrs()
		ifaceInfoList = append(ifaceInfoList, IfaceInfo{NickName: byName.Name, IPv4: address[1].String()})
	}

	// 打印设备信息
	// fmt.Println("Devices found:")
	// for _, device := range devices {
	// 	fmt.Println("\nName: ", device.Name)
	// 	fmt.Println("Description: ", device.Description)
	// 	fmt.Println("Devices addresses: ", device.Description)
	// 	for _, address := range device.Addresses {
	// 		fmt.Println("- IP address: ", address.IP)
	// 		fmt.Println("- Subnet mask: ", address.Netmask)
	// 	}
	// }
	var vaildIfaces []IfaceInfo
	for _, device := range devices {
		for _, address := range device.Addresses {
			for _, ifaceinfo := range ifaceInfoList {
				if strings.Contains(ifaceinfo.IPv4, address.IP.String()) {
					vaildIfaces = append(vaildIfaces, IfaceInfo{NPFName: device.Name, Description: device.Description, NickName: ifaceinfo.NickName, IPv4: ifaceinfo.IPv4})
					break
				}
			}
		}
	}

	return vaildIfaces
}

func main() {
	fmt.Println(get_if_list())
	opendevice()
}

func opendevice(){
	var (
		device       string = "\\Device\\NPF_{C6BAA07E-010E-499B-ADEA-0725304197D1}"
		snapshot_len int32  = 65536
		promiscuous  bool   = true
		err          error
		timeout      time.Duration = -1 * time.Second
		handle       *pcap.Handle
	)
	handle, err = pcap.OpenLive(device, snapshot_len, promiscuous, timeout)
	
	if err != nil {
		log.Fatal(err)
	}
	defer handle.Close()
	// 构造一个数据包源
	err = handle.SetBPFFilter("tcp")
    if err != nil {
        log.Fatal(err)
    }
	packetSource := gopacket.NewPacketSource(handle, handle.LinkType())

	// 读取包
	for packet := range packetSource.Packets() {
		//eth := packet.Layer(layers.LayerTypeEthernet)
        ip := packet.Layer(layers.LayerTypeIPv4)
      //  tcp := packet.Layer(layers.LayerTypeTCP)
       // udp := packet.Layer(layers.LayerTypeUDP)
        // 判断是否存在相应的协议信息
        // if eth != nil {
        //     fmt.Printf("Ethernet: %s\n", eth)
        // }
        if ip != nil {
			applicationLayer := packet.ApplicationLayer()
			if applicationLayer == nil { continue }
			payload := string(applicationLayer.Payload())
			if strings.HasPrefix(payload, "GET") || strings.HasPrefix(payload, "POST") {
				fmt.Printf("IPv4: %s\n", payload)
			}
			}
			//data:=strings(ip)
         
        // if tcp != nil {
        //     fmt.Printf("TCP: %s\n", tcp)
        // }
        // if udp != nil {
        //     fmt.Printf("UDP: %s\n", udp)
        // }

	}
}



