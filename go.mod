module Server-Core

go 1.14

require (
	github.com/AgoraIO/Tools/DynamicKey/AgoraDynamicKey/go/src v0.0.0-20231113100141-1e6d53fe7ce2
	github.com/bitly/go-simplejson v0.5.1
	github.com/fastly/go-utils v0.0.0-20180712184237-d95a45783239 // indirect
	github.com/gin-gonic/gin v1.9.1
	github.com/go-redis/redis/v8 v8.0.0-beta.6
	github.com/go-sql-driver/mysql v1.7.1
	github.com/go-xorm/xorm v0.7.9
	github.com/golang/protobuf v1.5.3
	github.com/google/gopacket v1.1.19
	github.com/gorilla/websocket v1.5.0
	github.com/jehiah/go-strftime v0.0.0-20171201141054-1d33003b3869 // indirect
	github.com/jmoiron/sqlx v1.2.0
	github.com/jonboulle/clockwork v0.2.0 // indirect
	github.com/lestrrat-go/file-rotatelogs v2.3.0+incompatible
	github.com/lestrrat-go/strftime v1.0.3 // indirect
	github.com/panjf2000/gnet v1.2.0
	github.com/rifflock/lfshook v0.0.0-20180920164130-b9218ef580f5
	github.com/sirupsen/logrus v1.6.0
	github.com/tebeka/strftime v0.1.5 // indirect
	github.com/ugorji/go v1.1.7 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
	google.golang.org/genproto v0.0.0-20200904004341-0bd0a958aa1d // indirect
	google.golang.org/grpc v1.31.1 // indirect
	google.golang.org/protobuf v1.31.0
	xorm.io/builder v0.3.13 // indirect
	xorm.io/xorm v1.3.4
)
